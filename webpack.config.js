'use strict';

var path = require('path');
var webpack = require('webpack');

var config = {
  entry: {
    test: './test.js',
  },

  output: {
    path: path.resolve(__dirname, './dist'),
    filename: '[name].bundle.js',
    chunkFilename: '[name].chunk.js',
  },

  module: {
    rules: [
      {
        test: /\_worker\.js$/,
        loader: 'worker-loader',
      },
    ],
  },

  plugins: [
    {
      apply: function(compiler) {
        compiler.plugin("compilation", function (compilation) {
          compilation.plugin("optimize-chunk-assets", function (chunks, callback) {
            if(chunks[0].files[0].indexOf('worker.js') !== -1) {
              compilation.errors.push(new Error("test"));
            }
            callback();
          });
        });
      },
    },
  ],
}

module.exports = config;
